package MonteCarloPack;

import java.util.List;

import class2.Pair;
import org.joda.time.DateTime;

public interface PathGenerator {
	
	public List<Pair<DateTime, Double>> getPath();

}
