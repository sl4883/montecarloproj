package MonteCarloPack;

import com.nativelibs4java.opencl.*;


import java.util.Arrays;
import java.util.Hashtable;
import java.util.Map;

public class maintest {
public static void main(String[] args) {

	int numsToGenerate;    //2 million
	numsToGenerate = 2000000;
	double rate =.0001;
	double sigma = 0.01;
	double S0 = 152.35;
	int N = 252; //number of days to run
	double K=165;
	double payout;
	
	//stopping criteria is with prob 96% the estimation error is less than 1%
	//we want area within 96% of the Gaussian (two-tailed) so z-score ~ 2.0539
	//epsilon error = 0.01
	
	double epsilon = 0.01;
	double zscore=2.0539;
	
	double myConst = zscore/epsilon;
	double largeNeuro=1e5; double largeNasian=1e5;
	
	MeanTracker euroMean = new MeanTracker();
	MeanTracker asianMean = new MeanTracker();
	
	int i=0;
	while ((largeNeuro > i) || (largeNasian > i)) {
	StockPrices myStockPrice = new StockPrices(rate, N, sigma, S0, numsToGenerate);
	EuropeanCallOption euroOption = new EuropeanCallOption(K);
	payout = euroOption.getPayout(myStockPrice);
	//System.out.println("Euro payout is: "+ payout);
	
	euroMean.addPayout(payout);
	
	
	AsianCallOption asianOption = new AsianCallOption(K);
	payout = asianOption.getPayout(myStockPrice);
	//System.out.println("Asian payout is: "+ payout);
	
	asianMean.addPayout(payout);
	
	if (i>10000) {
	largeNeuro = myConst*myConst * euroMean.getVar();
	largeNasian = myConst*myConst * asianMean.getVar();
	}
	
	i++; 
	
	if (i % 100000==0)
		{System.out.println(i); }
	}
	
	System.out.println("Euro Option Average is " + euroMean.getMean());
	System.out.println("Euro Option Variance: " + euroMean.getVar());
	System.out.println("Euro Option standard dev: " + Math.sqrt(euroMean.getVar()));
	System.out.println("Asian Option Average is " + asianMean.getMean());
	System.out.println("Asian Option Variance " + asianMean.getVar());
	System.out.println("Asian Option standard dev: " + Math.sqrt(asianMean.getVar()));
	
	
	System.out.println();
	System.out.println("Number of iterations needed for euro at current variance: " + largeNeuro);
	System.out.println("Number of iterations needed for asian at current variance: " + largeNasian);
	
	System.out.println("Number of iterations taken = " + i);
}
}
