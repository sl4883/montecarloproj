package MonteCarloPack;

import java.util.List;

import org.joda.time.DateTime;

import class2.Pair;

public class StockPrices implements Path {
	List<Pair<DateTime, Double>> STOCK_PRICE_PATH;
	double[] prices;
	int key = 0;
	double rate;
	int N;
	double sigma;
	double S0;
	int numToGen = 2000000;


	StockPrices(
				double rate,
				int N,
				double sigma,
				double S0,
				int numsToGen) {
		this.prices = new double[N];
		this.N=N;
		this.sigma=sigma;
		this.S0=S0;
		numToGen=numsToGen;

		this.rate=rate;
		RandVectGen rvg = new RandVectGen();
		DateTime startDate = new DateTime(0, 1, 1, 0, 0);
		DateTime endDate = startDate.plusDays(N);
		GBMRandomPathGenerator Psy = new GBMRandomPathGenerator(
				 rate,  N,
				 sigma,  S0,
				 startDate,  endDate,
				 rvg, numToGen);
		this.STOCK_PRICE_PATH=Psy.getPath();
		for (int i=0; i<numToGen; i++) {
		Pair<DateTime, Double> indivPair = STOCK_PRICE_PATH.get(i);
		prices[i]=indivPair.getValue();
		}
		}



	@Override
	public double[] getPrices() {
		// TODO Auto-generated method stub

		return prices;}
}
