package MonteCarloPack;

public interface PayOut {
	
	public double getPayout(Path path);

}
