package MonteCarloPack;

import java.util.LinkedList;
import java.util.List;

import class2.Pair;
import org.joda.time.DateTime;

public class GBMRandomPathGenerator implements PathGenerator {
	
	private double rate;
	private double sigma;
	private double S0;
	private int N;
	private DateTime startDate;
	private DateTime endDate;
	private RandomVectorGenerator rvg;
	int numToGen;
	
	public GBMRandomPathGenerator(double rate, int N,
			double sigma, double S0,
			DateTime startDate, DateTime endDate,
			RandomVectorGenerator rvg, int numToGen){
		this.startDate = startDate;
		this.endDate = endDate;
		this.rate = rate;
		this.S0 = S0;
		this.sigma = sigma;
		this.N = N;
		this.rvg = rvg;

	}

	@Override
	public List<Pair<DateTime, Double>> getPath() {
		double[] n = rvg.getVector(numToGen);
		DateTime current = new DateTime(startDate.getMillis());
		long delta = (endDate.getMillis() - startDate.getMillis())/N;
		List<Pair<DateTime, Double>> path = new LinkedList<Pair<DateTime,Double>>();
		path.add(new Pair<DateTime, Double>(current, S0));
		for ( int i=1; i < numToGen; ++i){
			current.plusMillis((int) delta);
			path.add(new Pair<DateTime, Double>(current, 
					path.get(path.size()-1).getValue()*Math.exp((rate-sigma*sigma/2)+sigma * n[i-1])));
		}
		return path;
	}
	

	

}
