package MonteCarloPack;

public class AsianCallOption implements PayOut {

		private double K;
		
		public AsianCallOption(double K){
			this.K = K;
		}

		@Override
		public double getPayout(Path path) {
			double[] prices = path.getPrices();
			double tempcounter=0;
			for (int i=0; i<prices.length; i++)
				tempcounter+=prices[i]; 
			
			double average = tempcounter/prices.length;
			return Math.max(0, average - K);
		}

	}
