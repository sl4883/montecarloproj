package MonteCarloPack;

import com.nativelibs4java.opencl.*;
import com.nativelibs4java.opencl.util.ParallelRandom;
import java.io.*;

/**
 * Created by Scott on 12/14/2014.
 */
public class arraygen {
    int batchsize = 2000000;

    public static void main(String[] stuff) throws IOException {

        // Creating the platform which is out computer.
        CLPlatform clPlatform = JavaCL.listPlatforms()[0];
        // Getting the GPU device
        CLDevice device = clPlatform.getBestDevice();
        //CLDevice device = clPlatform.listGPUDevices(true)[0];
        // Verifing that we have the GPU device
        System.out.println("*** New device *** ");
        System.out.println("Vendor: " + device.getVendor());
        System.out.println("Name: " + device.getName());
        System.out.println("Type: " + device.getType());
        // Let's make a context
        CLContext context = JavaCL.createContext(null, device);
        // Lets make a default FIFO queue.
        CLQueue queue = context.createDefaultQueue();

        double[] numbers = new double[2000000];
        int i = 0;

        int batchsize = 20000;

        ParallelRandom r = new ParallelRandom(queue, batchsize, 200);
        r.setPreload(true); // faster
        while (i < 2000000) {
            if ((i % (batchsize - 1) != 0) && (i == 0)) {
                r = new ParallelRandom(queue, batchsize, 200);
                numbers[i] = r.nextDouble();
                r.next();
                i++;
            } else {
                numbers[i] = r.nextDouble();
                i++;
            }
        }
    }


}
