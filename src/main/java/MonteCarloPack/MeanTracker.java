package MonteCarloPack;

public class MeanTracker {

public double runningSum = 0;
public double xisquares = 0;
public double runningMean = 0;
public int n = 0;
double mean = 0;

public void addPayout(double payout) {
	runningSum+=payout;
	xisquares+=payout*payout;
	n++;
}

public double getMean() {
	if (n != 0) 
	mean = runningSum / n;
	return mean;
	}
	
public double getXiSq_N() {
	double xisq_N = 0;
	if (n != 0) 
	xisq_N = xisquares / n;
	return xisq_N;
	}

public double getVar() {
	double var;
	var = xisquares/n - (runningSum/n)*(runningSum/n);
	return var;
}
	
public int getN() {
	return n;
}

public double getXiSquares() {
	return xisquares;
}



}
