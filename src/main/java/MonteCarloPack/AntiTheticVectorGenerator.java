package MonteCarloPack;

import java.util.Arrays;

public class AntiTheticVectorGenerator implements RandomVectorGenerator{
	
	private RandomVectorGenerator rvg;
	double[] lastVector;
   	int numGen;

	public AntiTheticVectorGenerator(RandomVectorGenerator rvg, int numToGen){
		this.rvg = rvg;
		numGen=numToGen;
	}


	@Override
	public double[] getVector(int numGen) {
		if ( lastVector == null ){
			lastVector = rvg.getVector(numGen);
			return lastVector;
		} else {
			double[] tmp =Arrays.copyOf(lastVector, lastVector.length);
			lastVector = null;
			for (int i = 0; i < tmp.length; ++i){ tmp[i] = -tmp[i];}
			return tmp;
		}
	}
	
}

