package MonteCarloPack;

public interface RandomVectorGenerator {
	
	public double[] getVector(int numsToGen);
//returns a 252 element vector of normally distributed numbers
	
}
