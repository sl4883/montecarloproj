package MonteCarloPack;
import com.nativelibs4java.opencl.*;

import java.util.Random;

public class RandVectGen implements RandomVectorGenerator {
	public double[] newVector;
	int vectorused=0;
	int numsToGen = 2000000; //2 millin
	

	public double[] getVector(int numGen) {
		if (vectorused == 1 ) {
			vectorused = 0;
			for (int i=0; i<newVector.length; i++) {
				newVector[i]= -newVector[i];
			}
			}

		numsToGen=numGen;

		if (vectorused != 1) {
		double mean=0.0; double sigma=1.0;
		Random rng = new Random();
		newVector = new double[numsToGen];
		vectorused=1;
		for (int i=0; i<numsToGen; i++) {
			newVector[i]=0;
			newVector[i]=mean + sigma * rng.nextGaussian();
		}
	 }
		return newVector;
}
}